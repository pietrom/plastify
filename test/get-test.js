const assert = require('assert')
const { plastify } = require('../index')
const axios = require('axios')

const { describe, it, before, after } = require("mocha")

function getRandom(min, max) {
    return Math.floor(min + Math.random() * (max - min))
}

function getAvailableRandomPort() {
    return getRandom(10000, 10500)
}

const config = {
    handlers: {
        'GET': {
            '/hello': (request, response) => ({
                status: 200,
                contentType: 'application/json',
                payload: { message: 'Hello, World!' }
            })
        }
    },
    defaultHandler: (request, response) => ({
        status: 404,
        contentType: 'application/json',
        payload: { error: `No handler available for ${request.method} ${request.url}` }
    })
}

describe('app', function() {
    let app, port, http

    before(() => {
        port = getAvailableRandomPort()
        app = plastify( config)
        app.start(port)
        http = axios.create({ baseURL: `http://localhost:${port}`, validateStatus: (st) => st >= 200 && st < 500 })
    })

    after(() => {
        if(app) {
            app.stop()
        }
    })

    function get(url) {
        return http.get(url)
    }

    function post(url) {
        return http.post(url)
    }

    function getHeader(response, key) {
        const search = key.toLowerCase()
        for (const keyElement of Object.keys(response.headers)) {
            if(keyElement.toLowerCase() === search) {
                return response.headers[keyElement]
            }
        }
        return null
    }

    it('should expose get API', async () => {
        const response = await get('/hello')
        assert.equal(response.status,200)
        assert.deepEqual(response.data, { message: 'Hello, World!' })
    })

    it('should use default handler for not existing routes', async () => {
        const response = await get('/not-existing')
        assert.equal(response.status,404)
        assert.deepEqual(response.data, { error: `No handler available for GET /not-existing` })
    })

    it('supports route handler addition at runtime', async () => {
        app.get('/added/route', (req, res) => ({ status: 204, contentType: 'application/json' }))
        const response = await get('/added/route')
        assert.equal(response.status,204)
    })

    it('uses application/json as default content type', async () => {
        app.get('/default-is-json', (req, res) => ({ status: 204 }))
        const response = await get('/default-is-json')
        assert.equal(getHeader(response, 'Content-Type'),'application/json')
    })

    it('uses 200 as default status', async () => {
        app.get('/default-is-ok', (req, res) => ({ payload: { x : 1 } }))
        const response = await get('/default-is-ok')
        assert.equal(response.status,200)
    })

    it('uses 204 as default status when there is no payload', async () => {
        app.get('/default-is-no-content', (req, res) => ({}))
        const response = await get('/default-is-no-content')
        assert.equal(response.status,204)
    })

    it('uses 201 as default status when method is POST', async () => {
        app.post('/default-is-created', (req, res) => ({}))
        const response = await post('/default-is-created')
        assert.equal(response.status,201)
    })
})
