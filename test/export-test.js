const assert = require('assert')

const {describe, it} = require("mocha")

describe('index.js', function() {
    it('should export plastify', function() {
	const { plastify } = require('../index')
        assert.notEqual(plastify, null)
    })
})
