const http = require('http')
const { createHttpTerminator } = require('http-terminator')



async function close(signal) {
    console.info(`${signal} signal received at`, now());
    await httpTerminator.terminate()
}

function getDefaultStatus(request, result) {
    if (request.method === 'POST') {
        return 201
    }
    return result.payload ? 200 : 204
}

module.exports = function plastify(config) {
    let handlers = config.handlers
    let server, httpTerminator

    function addHandler(method, url, handler) {
        handlers[method] = handlers[method] || []
        handlers[method][url] = handler
    }

    const supportedMethods = ['GET', 'POST']
    return {
        start: (port) => {
            server = http.createServer(function (req, res) {
                const handler = handlers[req.method][req.url] || config.defaultHandler
                const result = handler(req, res)
                const status = result.status || getDefaultStatus(req, result)
                const contentType = result.contentType || 'application/json'
                res.writeHead(status, {'Content-Type': contentType })
                if(result.payload) {
                    const realPayload = contentType === 'application/json' ? JSON.stringify(result.payload) : result.payload
                    res.write(realPayload)
                }
                res.end()
            }).listen(port)

            httpTerminator = createHttpTerminator({
                server
            })
        },
        stop: async () => {
            await httpTerminator.terminate()
        },
        ...supportedMethods.reduce((acc, curr) => ({...acc, [curr.toLowerCase()]: (url, handler) => addHandler(curr, url, handler)}), {})
    }
}